<?php

#####################################################
# MODULE: 	modal.php  
# PURPOSE: 	shows a php file as a popup.
# USAGE: 	private
# USED BY:  box.php (via xhr)
# LICENSE:  MIT or equivalent
# AUTHOR:   denjell
# VERSION:  0.6.1
# PROJECT:	ARTBOX.IO
#####################################################

?>

<div id="modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="min-width:24em !important;margin-bottom:15em">


<div class="modal-header playing" style="background-color:#fff !important">
<span style="margin:0.3em 0 0 0">
<img class="pull-right" style="margin-top:-0.2em" src="<?php print constant("SYSURL") ?>/img/artboxlogo_small.png"/>

<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="$('#modal').modal('toggle');
"><i class="icon-remove"></i></button>
<button id="fullsizeModal" class="btn"><i class="icon-plus"></i></button>


</span>
</div>
<div class="modal-body">
<div class="modalLoading">
	<p>
		ANALYZING FILE
	</p>
</div>
</div>


</div>
<!-- crazy hack to prevent the background from scrolling when the modal is open 
http://stackoverflow.com/questions/9538868/prevent-body-from-scrolling-when-a-modal-is-opened
-->
<style>
body.modal-open {
    overflow: hidden;
}
</style>
<script>
function printer(){

	var Print = window.open("printPage","_blank","width=450,height=470,left=400,top=100,menubar=yes,toolbar=no,location=no,scrollbars=yes");
	Print.document.open();
	Print.document.write("<!doctype html><html><head><title>Print<\/title><link rel=\"stylesheet\" href=\"<?php print constant("SYSURL") ?>/css/table.css\" \/><\/head><body onload=\"print();\">" + $('#everything').html() + "<div id=\"footer\"><img src=\"<?php echo constant("SYSURL")?>\/img\/artboxlogo_small.png\"><\/br>http:\/\/artbox.io  <a href=\"#print\" onclick=\"print()\">PRINT<\/a><\/div><\/body><\/html>");
	Print.document.close()
}



$(function(){
$('#modal').on('hidden', function(){
	$(this).removeData('modal');
	$('#modal .modal-body').html('<div class="modalLoading"><p>ANALYZING FILE</p></div>');
});

$("#fullsizeModal").on("click",function(){
	$("#modal").toggleClass('fullsizeModal');
	$('#fullsizeModal i').toggleClass('icon-plus icon-minus');
	$(".modal-body").toggleClass('fullsizeModalBody');
})

})
</script>