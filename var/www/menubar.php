<?php # menubar.php ?>
 <div class="navbar navbar-fixed-top">
    <div class="navbar-inner" style="overflow:hidden">
        <div class="container-fluid">
            <div id="logoHolder" class="navbar-text left" style="margin-left:1em">
				<a class="title" href="<?php print constant("SYSURL") ?>" >
					<img id="mainLogo" src="<?php print constant("SYSURL") ?>/img/artboxlogo_small.png"/>
				</a>
			</div>
      	</div>
    </div>
</div>