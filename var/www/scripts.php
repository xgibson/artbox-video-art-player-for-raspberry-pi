<?php //scripts.php ?>
<!-- Placed at the end of the document so the pages load faster -->

<script>


$(function(){
<?php print $noprivilegeJQ ?>

  // Setup drop down menu
 // $('.dropdown-toggle').dropdown();
 
  // Fix input element click problem
  $('.dropdown #signin input, .dropdown #signin label, .dropdown #signin a, .dropdown #signin button, .dropdown #signin, .dropdown #signin:not(.info), .search, #searchDropdown, .combobox').click(function(e) {
    e.preventDefault();
  });
/*

*/
 $(".toggle").hover(
    function(){
        var thisdiv = $(this).attr("data-target")
        $(thisdiv).collapse("show");
    },
    function(){
        var thisdiv = $(this).attr("data-target")
        $(thisdiv).collapse("hide");
    }
);
// switch click for mouseover on accordion
    $('#accordion2').on('mouseenter.collapse.data-api', '[data-toggle=collapse]', function(e) {
        var $this = $(this),
            href, target = $this.attr('data-target') || e.preventDefault() || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') //strip for ie7
            ,
            option = $(target).data('collapse') ? 'show' : $this.data()
            $(target).collapse(option)
    })

$('#modal').on('hidden' ,function(){      
  $("video").each(function(){
    $(this)[0].pause();
  })
  $("audio").each(function(){
    $(this)[0].pause();
  })
  $(this).removeData('modal');
 // $(this).modal({remote: ""});
})


//$('.<?php print $action?>').addClass('active');  

   
$('.panel').draggable({ 
  handle: ".move, .backing", 
  containment: "#boundingFrame"}).on("mouseover",function(){
  if ($('.blur').css('display')!='block') {
  $(".blur").effect('slide',{direction:'down'},200).fadeIn(100);
}
})


$('*').not(".minitool, #Volltext").on("focus", function() {

    $(".message").fadeOut(500);

  });


})

</script>