
var Event = function(beschreibung, className) {
    this.beschreibung = beschreibung;
    this.className = className;
    this.href="#"+className;
};

var events = {};
//events[new Date("04/18/2013")] = new Event("Something nice, long description, maybe even a picture. We could make this very long, but will have to look into escaping things like # / etc.", "Loge");
//events[new Date("04/30/2013")] = new Event("Party in der Fabrique", "Fabrique");


  $(function() {
    $( document ).tooltip({
      position: {
        my: "center bottom-20",
        at: "center top",
        using: function( position, feedback ) {
          $( this ).css( position );
          $( "<div>" )
            .addClass( "arrow" )
            .addClass( feedback.vertical )
            .addClass( feedback.horizontal )
            .appendTo( this );
        }
      }
    });
  });

  (function( $ ) {
    $.widget( "ui.combobox", {
      _create: function() {
        var input,
          that = this,
          wasOpen = false,
          select = this.element.hide(),
          selected = select.children( ":selected" ),
          value = selected.val() ? selected.text() : "",
          wrapper = this.wrapper = $( "<span>" )
            .addClass( "ui-combobox" )
            .insertAfter( select );
 
        function removeIfInvalid( element ) {
          var value = $( element ).val(),
            matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( value ) + "$", "i" ),
            valid = false;
          select.children( "option" ).each(function() {
            if ( $( this ).text().match( matcher ) ) {
              this.selected = valid = true;
              return false;
            }
          });
 
          if ( !valid ) {
            // remove invalid value, as it didn't match anything
            $( element )
              .val( "" )
             // .attr( "title", value + " didn't match any item" )
             // .tooltip( "open" );
            select.val( "" );
            setTimeout(function() {
              input.tooltip( "close" ).attr( "title", "" );
            }, 2500 );
            input.data( "ui-autocomplete" ).term = "";
          }
        }
 
        input = $( "<input>" )
          .appendTo( wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "ui-state-default ui-combobox-input" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: function( request, response ) {
              var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
              response( select.children( "option" ).map(function() {
                var text = $( this ).text();
                if ( this.value && ( !request.term || matcher.test(text) ) )
                  return {
                    label: text.replace(
                      new RegExp(
                        "(?![^&;]+;)(?!<[^<>]*)(" +
                        $.ui.autocomplete.escapeRegex(request.term) +
                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                      ), "<strong>$1</strong>" ),
                    value: text,
                    option: this
                  };
              }) );
            },
            select: function( event, ui ) {
              ui.item.option.selected = true;
              that._trigger( "selected", event, {
                item: ui.item.option
              });
            },
            change: function( event, ui ) {
              if ( !ui.item ) {
                removeIfInvalid( this );
              }
            }
          })
          .addClass( "ui-widget ui-widget-content ui-corner-left" );
 
        input.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
          return $( "<li>" )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
 
        $( "<a>" )
          .attr( "tabIndex", -1 )
          .attr( "title", "Choose your filter" )
          .tooltip()
          .appendTo( wrapper )
          .button({
            icons: {
              primary: "icon-tags"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "ui-corner-right ui-combobox-toggle" )
          .mousedown(function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .click(function() {
            input.focus();
 
            // close if already visible
            if ( wasOpen ) {
              return;
            }
 
            // pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
 
        input.tooltip({
          tooltipClass: "ui-state-highlight"
        });
      },
 
      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
  $('.ui-combobox-toggle').html('<i class="icon-tags"></i>');

  })( jQuery );
 


$(function() {
    var availableTags = [
      "ActionScript",
      "AppleScript",
      "Asp",
      "BASIC",
      "C",
      "C++",
      "Clojure",
      "COBOL",
      "ColdFusion",
      "Erlang",
      "Fortran",
      "Groovy",
      "Haskell",
      "Java",
      "JavaScript",
      "Lisp",
      "Perl",
      "PHP",
      "Python",
      "Ruby",
      "Scala",
      "Scheme"
    ];
    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#Tags" )
      // don't navigate away from the field on tab when selecting an item
      .bind( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).data( "autocomplete" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        minLength: 0,
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          response( $.ui.autocomplete.filter(
            availableTags, extractLast( request.term ) ) );
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      });
  });

var tooltips = $( "[title]" ).tooltip();
$(document).ready(function(){
	$('.blag').bind("focus", function(){
       tooltips.tooltip( "open" );
		e.preventDefault();
	});
	$('.info').bind("mouseenter mouseleave", function(e){
		e.preventDefault();
	});

	$('form').bind("submit", function(){
		console.log($('form#event').serializeArray());
		$('#Publish').slideDown(300)
	});



// call the datepicker on the following
  $(".datepicker")
    .focus(function() { // turn off android keyboard!
    this.blur();
    })
    .will_pickdate({
      timePicker: false,
      allowEmpty: true,
      format: 'd. M Y',
      inputOutputFormat: 'd. M Y'
    });

  $(".timepicker")
    .focus(function() { // turn off android keyboard!
      this.blur();
      })
    .will_pickdate({
      timePickerOnly: true,
      allowEmpty: true,
      format: 'H:i',
      inputOutputFormat:'H:i'
  })

  $(".datetimepicker")
    .focus(function() { // turn off android keyboard!
    this.blur();
    })
    .will_pickdate({
      timePicker: true,
      allowEmpty: true,
      format: 'd. M Y - H:i',
      inputOutputFormat:'d. M Y - H:i'
    });

// sets objects with quantity class (we assume they are inputs)
// to use the mousewheel api

$('.quantity').on("focus", function (){
    $(this).mousewheel(function(event, delta, X, Y) {
      var val=$(this).val();
      if (delta < 0) {
        if (val == 0) {
          return
        } else {
          val = val +++ delta;
        }
      } else {
        val = val +++ delta;
      }
      //console.log(val, delta);
      $(this).val(val);
      event.preventDefault();
    });
  });







}); // end document ready