<?php

#####################################################
# MODULE: 	pseudoshell.php
# PURPOSE: 	makes passthru access to bash from the webinterface.
# USAGE: 	pseudoshell.php post {command,time,browser,password}
# USED BY:  box.php (via xhr)
# LICENSE:  MIT or equivalent
# AUTHOR:   denjell
# VERSION:  0.6.3
# PROJECT:	ARTBOX.IO
#####################################################


function blacklist($needle,$haystack){
	if (strpos($haystack,$needle) === false){ } else { echo "<strong>".$needle."</strong> has been blacklisted."; die();}
}


function scheduler($command){
	if (strpos($command,"scheduler") === false){  } else {
		# we have to double flush because scheduler can't flush itself
		exec('/home/pi/.artbox/bin/flush');
		exec('bash -c "exec nohup setsid /home/pi/.artbox/bin/'.$command.' > /dev/null 2>&1 &"');
		echo '<hr><small>System says: "Scheduler" successful.</small>';
		die();}
}

function tvservice($command){
	if (strpos($command,"hdmi off") === false){  } else {
		exec('/opt/vc/bin/tvservice -o');
		echo "HDMI Turned off";
		die();
	}
	if (strpos($command,"hdmi on") === false){  } else {
		exec('/opt/vc/bin/tvservice -p');
		echo "HDMI Turned on";
		die();
	}
	if (strpos($command,"hdmi modes") === false){  } else {
		echo "Type e.g. 'CEA 4 HDMI' to set mode 4 <br>";
		exec('/opt/vc/bin/tvservice --modes=CEA 2>&1 > /tmp/modes.txt');
		system('cat /tmp/modes.txt',$return);
		die();
	}
	if (strpos($command,"hdmi ninja") === false){  } else {
		exec('/opt/vc/bin/tvservice -o;mode=\$(/opt/vc/bin/tvservice --modes=CEA | grep native | cut -d":" -f1 | cut -d" " -f5); tvservice --explicit="CEA \$mode HDMI"');	
		die();
	}
	if (strpos($command,"composite ntsc") === false){  } else {
		exec('/opt/vc/bin/tvservice -o; tvservice --sdtvon="NTSC 4:3"');
		echo "Composite Output NTSC";
		die();
	}
	if (strpos($command,"composite pal") === false){  } else {
		exec('/opt/vc/bin/tvservice -o; tvservice --sdtvon="PAL 4:3"');
                echo "Composite Output PAL";
		die();
	}
	if (strpos($command,"CEA ") === false){  } else {
		exec('/opt/vc/bin/tvservice --explicit="$command"');
		die();
	}
}

$command 		= $_POST['arg'];
$time 			= $_POST['time'];
$browser		= $_POST['browser'];
$password 		= $_POST['password'];
$extraCommand 		= $_POST['extraCommand'];

if ($_POST['su']=="off") { 
# lets blacklist first
blacklist("sudo ", $command);
blacklist("php ", $command);
blacklist("perl", $commmand);
blacklist("df ", $commmand);
blacklist("rm ", $commmand);
blacklist("su ", $command);
blacklist("ifconfig ", $command);
blacklist("dd ",$command);
blacklist("mkfs ", $command);
blacklist("wget ", $command);
blacklist("perl ", $commmand);
blacklist("chmod ", $commmand);
blacklist("chown ", $commmand);
blacklist("chgrp ", $commmand);
blacklist("> /dev/sda", $commmand);
blacklist("> /dev/sdb", $commmand);
}
scheduler($command);
tvservice($command);



# whitelist of pseudo bashs
switch ($command) {

	case "":
		$command2=$command;


	break;
	case "next":
		$command2=$command;
		$command='/home/pi/.artbox/bin/fifodo "Next" "o"';
	break;
	case "back":
		$command2=$command;
		$command='/home/pi/.artbox/bin/fifodo "Back" "i"';
	break;
	case "play":
	case "pause":
	case "playpause":
		$command2=$command;
		$command='/home/pi/.artbox/bin/fifodo "Play / Pause" "p"';
	break;
	case "volup":
		$command2=$command;
		$command='/home/pi/.artbox/bin/fifodo "Volume Up" "+"';
	break;
	case "voldn":
		$command2=$command;
		$command='/home/pi/.artbox/bin/fifodo "Volume Down" "-"';
	break;
	case "halt":
		$command2=$command;
		$command="/home/pi/.artbox/bin/flush";
	break;
	case "shutdown":
		$command2=$command;
		$command="sudo halt ; echo 'Shutting down now. Goodbye.'";
	break;
	case "restart":
		$command2=$command;
		$command="sudo shutdown now -r ; echo 'Going for a Restart now. See ya!'";
	break;
	case "man artbox":
		$command2=$command;
		$command="cat /home/pi/.artbox/manual.txt";
	break;
	case "versions":
		$command2=$command;
		$command="cat /home/pi/.artbox/locks/versionhistory.lock";
	break;
	case "passwd":
		$command2=$command;
		$command='/home/pi/.artbox/bin/passwords "'.$password.'"';
			if ($password != "Change Password" && strlen($password) >= 8) {
				system($command);
				echo '<hr><strong>Password changed.</strong>';
				$log = 'echo "'.$time.' | '.$browser.' | '.$_SERVER['REMOTE_ADDR'].' | \"'.$command2.'\"" >> /home/pi/.artbox/logs/pseudoshell.log;';
				shell_exec($log);
			} else {
				echo 'Your password must be at least eight characters long.';
			}
			exit();
	break;
	case "purge playlists":
		$command2=$command;
		$command="rm /home/pi/playlists/* ; echo 'All Playlists cleared.'";
	break;
	case "network client":
		$command2=$command;
		$command="sudo cp -f /etc/network/interfaces_DHCP_client etc/network/interfaces ; echo 'Restart to discover new Artbox IP.' "; // sudo services restart ;
	break;
	case "network router":
		$command2=$command;
		$command="sudo cp -f /etc/network/interfaces_DHCP_router etc/network/interfaces ; echo 'Restart to discover new Artbox IP.'";
	break;
	case "sync":
		$command2=$command;
		# we add one second here to account for latency
		$command="sudo date -s '+1 sec ".$time." ' ";
	break;
	case "update":
		echo "Your connection to Artbox will freeze. Please wait.";
		$command2=$command;
		$command="/home/pi/.artbox/bin/updater";
	break;
	default:
		$command2=$command;
	break;
}

if (!isset($command)) {
	echo "No command entered.";
} else {
	//$escaped_command = escapeshellcmd($command);
	if(system($command, $return)) {
		echo '<hr><small>System says: "'. $command2 .'" successful.';
		$log = 'echo "'.$time.' | '.$browser.' | '.$_SERVER['REMOTE_ADDR'].' | \"'.$command2.'\"" >> /home/pi/.artbox/logs/pseudoshell.log;';
		shell_exec($log);
	} else {
		echo '<hr><small>System says: "'. $command2 .'" reported nothing.</small>';
	}
};
