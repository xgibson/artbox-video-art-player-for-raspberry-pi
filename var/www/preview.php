
<?php


$target =urldecode($_GET['target']);
$rootPath = "/";
$rootUrl = constant("SYSURL")."/mount/" ;

function icon($target){
$command ="exiftool -l -h -filename -directory -filesize -filetype -duration -videoframerate -avgbitrate -imagesize -sourceimagewidth -sourceimageheight -mimetype -encoder -compressorid -audioformat -audiosamplerate -audiobitrate -samplerate -audiochannels -channelmode /".$target;
$escaped_command = escapeshellcmd($command);
$result=system($escaped_command);
}

// Previewer
$ext="";
$len=0;
foreach (explode('/', $request) as $unit){
$len=$len+1;
if ($len == 3) {
  $ext = $unit;
}
if ($len == 5) {
  $file = $unit;
}
}
echo '
<div class="modalButtons" style="padding-bottom:1em">

<a id="print" href="#print" class="btn btn-mini" onclick="printer()"><i class="icon-print"></i></a> 
<a id="download" href="#download" class="btn btn-mini" ><i class="icon-arrow-down"></i></a> 
<a id="fullscreen" href="#fullscreen" class="btn btn-mini hidden" onclick="$(\'.media\')[0].play()"><i class="icon-fullscreen"></i></a> 
<a id="loopplay" href="#loopplay" class="btn btn-mini hidden" onclick="$(\'.media\')[0].play()"><i class="icon-refresh"></i></a> 
<a id="playbutton" href="#" class="playing btn btn-mini btn-success hidden"><i id="" class="icon-play"></i></a>
<span class="label" id="timecode" style="padding:0.1em"></span>
</div>
<div id="everything">';
  $ext=strtolower($ext);
	switch ($ext) {

	case "mp3":
	case "wav":
	case "ogg":
	echo '

<audio id="media" class="media" type="audio/'.$ext.'" >
	<source	src="'.$rootUrl.$target.'" type="audio/'.$ext.'" >
</audio>
<div> 
';
$type="sound";
icon($target);
echo '</div>';
	break;
	case "mp4":
		//$ext = "codecs=avc1.42E01E,mp4a.40.2"
	case "mpg":
	case "webm":
	case "mov":
//	echo mime_content_type(filename);
	echo '

<video id="media" class="media" type="video/'.$ext.'" width="100%" >
	<source	src="'.$rootUrl.$target.'" type="video/'.$ext.'" >
</video>

<div> 
';
$type="video";
icon($target);
echo '</div>';
	break;
	case "jpg":
	case "png":
	case "gif":
	case "tif":
	echo mime_content_type(constant("SYSPATH").$target);
	echo '<p style="text-align:center;"><img  id="media" src="'.$rootUrl.$target.'?'.time().'" style="position:relative;max-width:100%;margin:auto;"></p>
	<div> 
';
	$type="sound";
	icon($target);
	echo '</div>';
	break;
	default:
	echo '
  <p>'.$target.'</p>
	<h1 style="color:black">FILETYPE NOT RECOGNIZED!</h1>
	';

	break;
}


?>

</div>

<script>  function goFullscreen(id) {
    // Get the element that we want to take into fullscreen mode
    var element = document.getElementById(id);
    
    // These function will not exist in the browsers that don't support fullscreen mode yet, 
    // so we'll have to check to see if they're available before calling them.
    
    if (element.mozRequestFullScreen) {
      // This is how to go into fullscren mode in Firefox
      // Note the "moz" prefix, which is short for Mozilla.
      element.mozRequestFullScreen();
    } else if (element.webkitRequestFullScreen) {
      // This is how to go into fullscreen mode in Chrome and Safari
      // Both of those browsers are based on the Webkit project, hence the same prefix.
      element.webkitRequestFullScreen();
   } else {
   	  // fuck checking. just go for it. 
   	  // if you aren't using chrome you didn't follow the directions. :P
   	  element.requestFullScreen();
   }
   // Hooray, now we're in fullscreen mode!
  }
function loopPlay(){
	//$('.media').;
	var myVideo=$('.media');
//	if (myVideo.prop("loop")) { // loop supported
//  myVideo.loop = true;
//} else { // loop property not supported
  myVideo.bind({'ended': function () {
    this.currentTime = 0;
    this.play();
  }


});
 // myVideo.play();

}
$(function() {

	$('#playbutton').addClass('hidden');
	//$('.media').addClass('hidden');
	$('#timecode').addClass('hidden');
	$('document').on('click', function(){
		printerPage.close();
	})	
	$('#fullscreen').on('click',function(){
		goFullscreen('media');
	});
	$('#loopplay').on('click',function(){
		loopPlay();
  $('#loopplay').toggleClass('btn-info');
  $('#loopplay i').toggleClass('icon-white');
	});
  $('.media').bind({
       'timeupdate':function () {

      //     var refreshIntervalId =  window.setInterval(function(){
              s = $('.media')[0].currentTime;
              h = s / 3600 >> 0;
              s = s % 3600;
              m = s / 60 >> 0;
              s = s % 60 >> 0;
  
              if (h.toString().length < 2) { h = "0" + h;}
              if (s.toString().length < 2) { s = "0" + s;}
              if (m.toString().length < 2) { m = "0" + m;}

        	  $('#timecode').html(h + ":" + m + ":" + s);
      //       },1000/500); 
        },
   'canplay':function(){
  	this.play();
	$('#playbutton').removeClass('hidden');
	$('.media').removeClass('hidden');
	$('#timecode').removeClass('hidden');
	$('#loopplay').removeClass('hidden');	
	$('#print').on('click',function(){
		$(".media")[0].pause();
	})
	$('video').each(function(){
		$('#fullscreen').removeClass('hidden');
	})
  },
  'paused': function() {
		    $('#playbutton i').removeClass('icon-pause');
            $('#playbutton i').addClass('icon-play');
            $('#playbutton').unbind().bind('click', function () {
                playPreview();
            });        
            //clearInterval(refreshIntervalId);
	
	},
    'playing': function() {
		    $('#playbutton i').removeClass('icon-play');
            $('#playbutton i').addClass('icon-pause');
            $('#playbutton').unbind().bind('click', function () {
                pausePreview();
            }); 
	
	},

	'ended':function () {
		    $('#playbutton i').removeClass('icon-pause');
            $('#playbutton i').addClass('icon-play');
            $('#playbutton').unbind().bind('click', function () {
                playPreview();
            });
        //clearInterval(refreshIntervalId);    
        }
        
})

    function playPreview() {
        var vid = $(".media")[0];
        var playButton = $('#playbutton');
        $(document).keydown(function (e) {
            if (e.keyCode === 32) { //spacebar
                pausePreview();
                e.preventDefault();
            }
        });
		$('#playbutton i').removeClass('icon-play');
        $('#playbutton i').addClass('icon-pause');
        playButton.unbind().bind('click', function () {
            pausePreview();
        });
        vid.play();
    }
    function pausePreview() {
        var vid = $(".media")[0];
        var playButton = $("#playbutton");
        $(document).keydown(function (e) {
            if (e.keyCode === 32) {
                playPreview(); //spacebar
                e.preventDefault();
            }
        });
		    $('#playbutton i').removeClass('icon-pause');
            $('#playbutton i').addClass('icon-play');
        playButton.unbind().bind('click', function () {
            playPreview();
        });
        vid.pause();
    }

  $('#download').attr('href','<?php echo constant("SYSURL")?>/Download/?file=<?php print urlencode($target) ?>');


  $('img.iconset').each(function(){
    var iconset=this.id, 
    badge=$(this).attr('data-badge'), 
    icon=$(this).attr('data-icon');
    badgeMaker(iconset, badge);
    iconMaker(iconset, icon);
  })

$(window).resize(function() {
	$("#modal").addClass('fullsizeModal');
	$('#fullsizeModal i').removeClass('icon-plus');
	$('#fullsizeModal i').addClass('icon-minus');
	$(".modal-body").addClass('fullsizeModalBody');
})


})


</script>
