<?php

#####################################################
# MODULE:   playlistmaker.php
# PURPOSE:  this creates a DSP-compatible playlist as well schedule file
# USAGE:    pass schedule type, playlist
# USED BY:  private < box.php
# LICENSE:  MIT or equivalent
# AUTHOR:   denjell
# VERSION:  0.6.1
# PROJECT:  ARTBOX.IO
#####################################################

$scheduleType	=	$_POST['scheduleType'];
$scheduleData	=	$_POST['scheduleData'];
$filelist 		=	$_POST['filelist'];
$target			= 	$_POST['target']; # either usb or artbox
$boxname 		=	preg_replace('/[^a-zA-Z0-9_-]/s', '', $_POST['boxname']); 

# catch the overzealous preg / 0-length playlist name
if (strlen($boxname)==0) $boxname="playlist";

if ($target == "USB") {
	$targetRoot = "/var/www/mount/media/usb/";
	$trueRoot = "/media/usb/";
	if (!is_writable($targetRoot."/test.test")) {
    	echo 'The USB Target is not writeable. Have you attached a USB Device?';
    	die();
	}
} else {
	$targetRoot = "/var/www/mount/home/pi/playlists/";
	$trueRoot = "/home/pi/playlists/";
	if (!is_writable($targetRoot)) {
    	echo 'The SD Card is not writeable. Is the read-only switch active?';
    	die();
	}	
}

if ($scheduleData == "undefined" || $scheduleData == "") {
	if ($scheduleType != "Infiniteloop") {
		$scheduleType="Infiniteloop";
		echo "No schedule data entered.";
		die();
	}
}

$completeSchedule =$scheduleData.$trueRoot.$boxname.".playlist";

#clear all existing playlists
exec('rm /home/pi/playlists/*');

# make the schedule
$sched=$targetRoot.$scheduleType.".schedule";
if ($fp = fopen( $sched, 'wb' )){$pass=1;} else {echo "Schedule creation failed. Did you set a schedule type?"; die();}
if (fwrite( $fp, $completeSchedule)) {$pass=1;} else {echo "Could not create schedule file."; die();}
if (fclose( $fp )) {$pass=1;} else {echo "Could not write to schedule file."; die();}

# make the playlist
$play=$targetRoot.$boxname.".playlist";
if ($fp = fopen( $play, 'wb' )){$pass=1;} else {echo "Playlist creation failed. Did you set a schedule type?"; die();}
if (fwrite( $fp, $filelist."\n")) {$pass=1;} else {echo "Could not create playlist file."; die();}
if (fclose( $fp )) {$pass=1;} else {echo "Could not write to schedule file."; die();}
$bashplay=$trueRoot.$boxname.".playlist";

#shell_exec('/home/pi/.artbox/bin/dsp & disown');
exec('bash -c "exec nohup setsid /home/pi/.artbox/bin/dsp > /dev/null 2>&1 &"');

echo '"'.$boxname.'" created.';
if ($scheduleType!="Infiniteloop"){
	echo'<button class="btn btn-mini btn-success" onclick="bash(\'scheduler '.$bashplay.'\' ,1, \'Now Playing.\')">Play Now as Infinite Loop!</button>';
}
