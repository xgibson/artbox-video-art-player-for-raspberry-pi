<?php 

#####################################################
# MODULE: 	api.php
# PURPOSE: 	navigate between requests
# USAGE: 	all files redirect here first  
# USED BY:  .htaccess
# LICENSE:  MIT or equivalent
# AUTHOR:   denjell
# VERSION:  0.6.1
# PROJECT:	ARTBOX.IO
#####################################################

error_reporting(0);
session_start();

define('SYSPATH',"/var/www");
define('SYSURL',"https://".$_SERVER['SERVER_NAME']);
$ApiSearchLength ="2";

# THIS IS DANGEROUS! MUST SET TO HOSTNAME / IP ADDRESS TO PREVENT CROSS SITE SCRIPTING.
header('Access-Control-Allow-Origin: https://'.$_SERVER['SERVER_NAME']); 

# GET THE SERVER VALS
$request 		= $_SERVER['REQUEST_URI'];
$path 			= $_SERVER['PATH_INFO'];
$server_name 	= $_SERVER['SERVER_NAME'];
$host 			= $_SERVER['HTTP_HOST'];
$user_agent 	= $_SERVER['HTTP_USER_AGENT'];
$user_ip 		= $_SERVER['REMOTE_ADDR'];
$time 			= $_SERVER['REQUEST_TIME'];
$session 		= session_id();

# don't log the timer!
if ($request != "/Timer") {
	$log_message = date('Y-m-d H:i.s')." | ".$user_ip." | ".$host.$request."\n";
	error_log($log_message, 3, "/home/pi/.artbox/logs/http.log");
}

# if it is just a post / put then don't bother sending any html back. 
# just process if possible, send a reply and send that reply back to the user
require_once __DIR__ . '/parse.php';


switch ($action) 
{
	case "Manual":
	require_once __DIR__ . '/mount/home/pi/manual.php';
	break;
	case "Pseudoshell":
	require_once __DIR__ . '/pseudoshell.php';
	break;
	# not working yet
	case "Upload":
	require_once __DIR__ . '/upload.php';
	break;
	case "Timer":
	require_once __DIR__ . '/timer.php';
	break;
	case "Playlistmaker":
	require_once __DIR__ . '/playlistmaker.php';
	break;	
	case "Download":
	require_once __DIR__ . '/download.php';
	break;
	case "Preview":
	require_once __DIR__ . '/preview.php';
	break;
	case "Filetree":
	require_once __DIR__ . '/jqueryFileTree.php';
	break;
	case "Validator":
		require_once __DIR__ . '/header.php';
		require_once __DIR__ . '/Validator.php';
	break;

    case "MailMan":
    require_once __DIR__ . '/mailman.php';
    break;
    case "Logout":
    break;
    default:

	// get the version number as set by our bootstrap
	$file = @fopen(constant("SYSPATH").'/version.txt', "r") ;
	    $version = fgets($file) ;
	fclose($file) ;
	require_once __DIR__ . '/header.php';
	require_once __DIR__ . '/menubar.php';
	require_once __DIR__ . '/box.php';
	require_once __DIR__ . '/modal.php';
	require_once __DIR__ . '/footer.php';
}