<?php 

#####################################################
# MODULE:   box.php
# PURPOSE:  this is the main interface
# USAGE:    point, click, darg etc.  
# USED BY:  private / api.php
# LICENSE:  MIT or equivalent
# AUTHOR:   denjell
# VERSION:  0.6.1
# PROJECT:  ARTBOX.IO
#####################################################

?>

<!-- ----------------------------------------------------------------------------------- -->
<div class="container-fluid">
  <div class="row-fluid">
        <div class="span7">
<div class="accordion"  id="accordion2">



<div class="accordion-group">
<div  class="accordion-heading"> 
<div id="scheduleToggle" class="accordion-toggle " data-toggle="collapse" data-parent="#accordion2" href="#collapse2">
<i class="icon-time"></i> SCHEDULE


</div>

</div>
<div id="collapse2" class="accordion-body collapse schedule">
<div class="accordion-inner" style="min-height:10em">
<div class="dropdown"> 
  <span class="dropdown-toggle btn btn-mini" id="dLabel" role="menu" data-toggle="dropdown">
    <span id="scheduleType"><span class="scheduleIcon">?</span> - <span class="scheduleLabel">Schedule Type</span></span>
     <b class="caret"></b>
  </span>
   <ul class="dropdown-menu scheduler" aria-labelledby="dLabel">
    <li class="Infiniteloop"><a class="Infiniteloop"><span class="scheduleIcon">&#8734;</span> - <span class="scheduleLabel">Infiniteloop</span></a></li>
    <li class="Daily"><a class="Daily"><span class="scheduleIcon">1x</span> - <span class="scheduleLabel">Daily</span></a></li>
    <li class="Weekly"><a class="Weekly"><span class="scheduleIcon">7x</span> - <span class="scheduleLabel">Weekly</span></a></li>
    <li class="Special"><a class="Special"><span class="scheduleIcon">!!!</span> - <span class="scheduleLabel">Special</span></a></li>
    <li class="Multiday"><a class="Special"><span class="scheduleIcon">><</span> - <span class="scheduleLabel">Multiday</span></a></li>
  </ul>  
  <i class="icon-info-sign" onclick="$('.infoSchedule').toggleClass('hidden')"></i>
  </div>
<script>
$(function(){
  $('ul.dropdown-menu.scheduler li').on('click',function(){
  var label=$(this).find('.scheduleLabel').html();
    $('#scheduleType').html($(this).find('a').html());
    $('#scheduleTypeLabel').html($(this).find('.scheduleIcon').html());
    $('.schedules .interface2').addClass('hidden');
    $('.schedules').each(function(){
      $(this).find("[data-type='" + label + "']").removeClass('hidden');
    })
   // console.log(label);
  })
})
$(function(){
  $('ul.dropdown-menu.stopper li').on('click',function(){
    $('.stopType span').html($(this).find('a span').html());
    $('table tr td input').removeClass("Stop Length Loops")
    .addClass($(this).find('a span').html());
    $('.timePick.off').mobiscroll('destroy');
    timePick();
    })
})


</script>





<div class="schedules">
<div class="hidden interface2 Infiniteloop" data-type="Infiniteloop">  
  <table class="scheduler" data-provides="rowlink">
    <thead><tr><td>Day</td><td class="on">Start</td><td class="off">Stop</td></tr></thead>
    <tr class="Everday"><td class="nolink Daily">ALL</td><td class="nolink on" ><a href="#">NOW</a></td><td class="nolink off"><a href="#">FALSE</a></td></tr>
  </table>
    <span class="infoSchedule hidden">

  This playlist schedule will run your Artbox each and every day and night until the Sun supernovas or you publish a new playlist.
</span>
</div>
<div class=" hidden interface2 Daily" data-type="Daily">
  <table class="scheduler">
    <thead><tr><td>Day</td><td class="on">Start</td><td class="off">
<span class="dropdown"> 
  <span class="dropdown-toggle stopper" id="dLabel2" role="menu"  data-toggle="dropdown">
    <span class="stopType"><span>Stop</span></span>
     <b class="caret"></b>
    </span>
   <ul class="dropdown-menu stopper" aria-labelledby="dLabel2">
    <li class="Stop stoptype"><a class="Stop"><img src="<?php print constant("SYSURL") ?>/images/clock_stop.png"> <span>Stop</span></a></li>
    <li class="Length stoptype"><a class="Length"><img src="<?php print constant("SYSURL") ?>/images/progressbar.png"> <span>Length</span></a></li>
    <li class="Loops stoptype"><a class="Loops"><img src="<?php print constant("SYSURL") ?>/images/counter.png"> <span>Loops</span></a></li>
  </ul>
</span>
    </td></tr></thead>
    <tr class="EACH"><td class="EACH"><a class="scheduler minicon"><img class="EXdelete" src="images/delete.png"></a><input value="ALL" disabled></td><td><input class="timePick on"/></td></td><td class="nolink off"><input class="off timePick Stop"/></td></tr>

  </table>
  <span class="infoSchedule hidden">
  This type of schedule will play every day from a start time until a stop time. You can set the stop time to be a time of day, a length of time or a number of loops.
  </span> 
    <label class="checkbox" for="Override">
    <input id="Override" class="check" name="Overide" type="checkbox">Override</input></label>
    

    <label class="checkbox" for="Interrupt">
    <input id="Interrupt" class="check" name="Interrupt" type="checkbox">Interrupt</input></label>
</div>
<div class="hidden interface2 Weekly" data-type="Weekly">

  <table class="scheduler">
    <thead><tr><td>Day</td><td class="on">Start</td><td class="nolink off">
<span class="dropdown"> 
  <span class="dropdown-toggle stopper" id="dLabel2" role="menu"  data-toggle="dropdown">
    <span class="stopType"><span>Stop</span></span>
     <b class="caret"></b>
    </span>
   <ul class="dropdown-menu stopper" aria-labelledby="dLabel2">
    <li class="Stop stoptype"><a class="Stop"><img src="<?php print constant("SYSURL") ?>/images/clock_stop.png"> <span>Stop</span></a></li>
    <li class="Length stoptype"><a class="Length"><img src="<?php print constant("SYSURL") ?>/images/progressbar.png"> <span>Length</span></a></li>
    <li class="Loops stoptype"><a class="Loops"><img src="<?php print constant("SYSURL") ?>/images/counter.png"> <span>Loops</span></a></li>
  </ul>
</span>
    </td></tr></thead>
    <tr class="MON">
    <td class="MON"><a class="scheduler minicon"><img class="EXdelete" src="images/delete.png"></a> MON</td><td class="on"><input class="timePick on"/></td></td><td class="nolink off"><input class="off timePick Stop"/></td></tr>
    <tr class="TUE">
      <td class="TUE"><a class="scheduler minicon"><img class="EXdelete" src="images/delete.png"></a> TUE</td><td class="on"><input class="on timePick"/></td><td class="nolink off"><input class="off timePick Stop"/></td></tr>
    <tr class="WED">
      <td class="WED"><a class="scheduler minicon"><img class="EXdelete" src="images/delete.png"></a> WED</td><td class="on"><input class="on timePick"/></td><td class="nolink off"><input class="off timePick Stop"/></td></tr>
    <tr class="THU">
      <td class="THU"><a class="scheduler minicon"><img class="EXdelete" src="images/delete.png"></a> THU</td><td class="on"><input class="on timePick"/></td><td class="nolink off"><input class="off timePick Stop"/></td></tr>
    <tr class="FRI">
      <td class="FRI"><a class="scheduler minicon"><img class="EXdelete" src="images/delete.png"></a> FRI</td><td class="on"><input class="on timePick"/></td><td class="nolink off"><input class="off timePick Stop"/></td></tr>
    <tr class="SAT">
      <td class="SAT"><a class="scheduler minicon"><img class="EXdelete" src="images/delete.png"></a> SAT</td><td class="on"><input class="on timePick"/></td><td class="nolink off"><input class="off timePick Stop"/></td></tr>
    <tr class="SUN">
      <td class="SUN"><a class="scheduler minicon"><img class="EXdelete" src="images/delete.png"></a> SUN</td><td class="on"><input class="on timePick"/></td><td class="nolink off"><input class="off timePick Stop"/></td></tr>
  </table>
  <span class="infoSchedule hidden">
  This type of schedule lets you set start and end times for each day of the week. You can set the stop time to be a time of day, a length of time or a number of loops.
  </span> 
      <label class="checkbox" for="Override">
    <input id="Override" class="check" name="Overide" type="checkbox">Override</input></label>
    

    <label class="checkbox" for="Interrupt">
    <input id="Interrupt" class="check" name="Interrupt" type="checkbox">Interrupt</input></label>
    
</div>
<div class=" hidden interface2 Special" data-type="Special">
  <table  class="scheduler">
    <thead><tr><td>Day</td><td class="on">Start</td><td class="off">
<span class="dropdown"> 
  <span class="dropdown-toggle stopper" id="dLabel2" role="menu"  data-toggle="dropdown">
    <span class="stopType"><span>Stop</span></span>
     <b class="caret"></b>
    </span>
   <ul class="dropdown-menu stopper" aria-labelledby="dLabel2">
    <li class="Stop stoptype"><a class="Stop"><img src="<?php print constant("SYSURL") ?>/images/clock_stop.png"> <span>Stop</span></a></li>
    <li class="Length stoptype"><a class="Length"><img src="<?php print constant("SYSURL") ?>/images/progressbar.png"> <span>Length</span></a></li>
    <li class="Loops stoptype"><a class="Loops"><img src="<?php print constant("SYSURL") ?>/images/counter.png"> <span>Loops</span></a></li>
  </ul>
</span>
    </td></tr></thead>
      <a href="#placeTime"><tr class="Date"><td class="Date"><input class="datePick"/></td><td class="on"><input class="on timePick"/></td><td class="nolink off"><input class="off timePick Stop"/></td></tr></a>
  </table>
    <span class="infoSchedule hidden">
    This type of schedule lets you set a specific time and date for the playlist. You can use it for example to run for a long number of minutes such as 6000 (100 hours)
  </span>
    <label class="checkbox" for="Override">
    <input id="Override" class="check" name="Overide" type="checkbox" checked>Override
    </label>
   

    <label class="checkbox" for="Interrupt">
    <input id="Interrupt" class="check" name="Interrupt" type="checkbox">Interrupt
    </label>
    
</div>
<div class=" hidden interface2 Multiday" data-type="Multiday">
  <table  class="scheduler">
    <thead><tr><td class="on">Start</td><td class="off">Stop</td></tr></thead>
    <tr class="Date"><td class="on"><input class="timePick Multiday"/></td><td class="nolink off"><input class="timePick Multiday"/></td></tr>
  </table>
  <span class="infoSchedule hidden">
  This type of schedule lets you set a long playlist that may span several days, weeks, months or years. Wow. That's a lot of art!!!
  </span>
  <label class="checkbox" for="Override">
    <input id="Override" class="check" name="Overide" type="checkbox" checked>Override
  </label>
  <label class="checkbox" for="Interrupt">
    <input id="Interrupt" class="check" name="Interrupt" type="checkbox">Interrupt
  </label>
</div>
</div> <!-- end schedules -->
</div>
</div>
</div>


<div class="accordion-group">
  <div  class="accordion-heading"> 
    <span class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">
      <i class="icon-list"></i> PLAYLIST
    </span>
  </div>
  <div id="collapse1" class="accordion-body collapse playlist">
    <div class="accordion-inner">
      <textarea id="box1" class="editable btn btn-mini" rows="1">First List</textarea>
      <div id="cart">
        <div class="holder"> 
          <ol id="sortable">
            <li class="placeholder">Drag Media Here</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</div>




    <div class="accordion-group">
      <div  class="accordion-heading"> 
        <div id="scheduleToggle" class="accordion-toggle " data-toggle="collapse" data-parent="#accordion1" href="#collapse3">
        # CONSOLE
        </div>

      </div>
      <div id="collapse3" class="accordion-body collapse console">
        <div class="accordion-inner console">
          <div class="navbar">
          <a id="activatePlaylist" class="btn btn-mini">
            <i class="icon-check"></i> Publish
            <span id="itemcount" class="label label-inverse playlist" >0</span>
            <span id="scheduleTypeLabel" class="label label-inverse playlist">?</span>
          </a>

<span class="dropdown consoleButtons"> 
            <span class="dropdown-toggle" id="dLabel3" role="menu"  data-toggle="dropdown">
              <a class="terminalButton btn btn-mini">
                <i class="icon-cog"></i> Config
              </a>
            </span>

          <ul class="dropdown-menu pull-left" aria-labelledby="dLabel3">
            <li>
              <a class="terminalButton" onclick="bash('sync',1,'Press Enter to sync the Artbox with your browser time.')">
                Sync Clocks
              </a>
            </li>
            <li class="disabled">
              <a class="terminalButton" onclick="bash('network',0,'Modifying network settings coming soon.')">
                Network
              </a>
            </li>
            <li>
              <input id="newPassword" name="newPassword" type="text" value="Change Password" style="-webkit-outline:none" min-length="8" placeholder="Change Password">
            </li>
          </ul>
        </span>        

        <span class="dropdown consoleButtons"> 
            <span class="dropdown-toggle" id="dLabel4" role="menu"  data-toggle="dropdown">
              <a class="terminalButton btn btn-mini">
                <i class="icon-off"></i> System
              </a>
            </span>

          <ul class="dropdown-menu pull-left" aria-labelledby="dLabel4">
            <li>
              <a class="terminalButton" onclick="bash('flush',0,'Press Enter to immediately halt playback.')">
                Halt Playback
              </a>
            </li>
            <li class="disabled">
              <a class="terminalButton" data-toggle="modal" data-target="#modal" href="<?php print constant("SYSURL"); ?>/Upload">
                Upload
              </a>
            </li>
            <li>
              <a class="terminalButton" onclick="bash('restart',0,'Press Enter to Restart.')">
                Restart Now
              </a>
            </li>
            <li>
              <a class="terminalButton" onclick="bash('shutdown',0,'Press Enter to Shutdown now.')">
                Power Off
              </a>
            </li>

          </ul>
        </span> 

          <span class="dropdown consoleButtons"> 
            <span class="dropdown-toggle" id="dLabel5" role="menu"  data-toggle="dropdown">
              <a class="terminalButton btn btn-mini">
                <i class="icon-info-sign"></i> Help
              </a>
            </span>

          <ul class="dropdown-menu pull-left" aria-labelledby="dLabel5">
            <li>
              <a class="terminalButton" onclick="bash('man artbox',1,'Press Enter to view the Handbook.')">
                Handbook
              </a>            
            </li>
            <li>
              <a class="terminalButton" onclick="bash('about',1,'Artbox...')">
                About
              </a>            
            </li>        
          </ul>
          </span>



<span id="ffbutton" href="#" class="right playing btn btn-mini " onclick="bash('next',1,'Next chapter / clip.')"><i id="" class=" icon-fast-forward"></i></span>
<a id="pbutton" href="#" class="right playing btn btn-mini btn-success" onclick="bash('playpause',1,'Play / Pause current video.')"><i class="icon-pause"></i><i class="icon-play"></i></a>
<a id="backbutton" href="#" class="right playing btn btn-mini" onclick="bash('back',1,'Rewind to beginning of this chapter / clip.')"><i id="" class=" icon-backward"></i></a>
<a id="volupbutton" href="#" class="right playing btn btn-mini" onclick="bash('volup',1,'Increase Volume.')"><i id="" class="icon-volume-up"></i></a>
<a id="voldownbutton" href="#" class="right playing btn btn-mini" onclick="bash('voldn',1,'Decrease Volume.')"><i id="" class="icon-volume-down"></i></a>

        </div>
<div class="dark pad1 coffee roundedBottom">

<div id="messages" class="coffee hidden"><span></span>
</div>
<div id="timeNow" class="coffee">
  <span class="coffee time">
    <span id="artboxLabel"><strong>ARTBOX</strong>&nbsp;&nbsp;<span id="version">v.<?php print $version ?></span><br><span id="serverTime"></span><br>
    <span id="browserLabel"></span><br><span id="browserTime"></span>
  </span>
</div>
<input class="coffee" id="terminal" type="textarea">
</div>

<span class="terminalResponse"></span>
        </div>
      </div>
    </div>


</div>
</div> <!-- end accordion -->


<h4 id="noscript">
  In order to use this web interface for the Artbox you must enable Javascript und use an HTML5 compliant browser. At the moment the only browser capable of playing back all media is Google Chrome. It is a <a href="https://www.google.com/intl/en/chrome/browser/">free download</a> and is standards compliant. Welcome to the future!
</h4>



<div class="accordion" id="accordion1" >
  <div class="span5">
    <div class="accordion-group"  style="position:fixed;">
      <div  class="accordion-heading" style=""> 
        <div id="scheduleToggle" class="accordion-toggle " data-toggle="collapse" data-parent="#accordion1" style="border:none!important;" href="#collapse0">
        <i class="icon-film"></i> MEDIA

        </div>

      </div>
      <div id="collapse0" class="accordion-body" >
        <div class="accordion-inner " style="border:none;background:transparent;position:fixed;overflow-x:hidden;overflow-y:auto;max-height:70%;height:70%;min-height:70%;min-width:35%;width:35%;max-width:35%;float:right;padding:0!important">
          <div id="browser" style="background:none!important;">
            <div>
              <div id="fileTree" style="background:none!important;">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<!-- ----------------------------------------------------------------------------------- -->




<!-- using this branch: https://github.com/koppor/jquery-ui-touch-punch -->

<script type="text/javascript">

function hideKeyboard(element) {
    element.attr('readonly', 'readonly'); // Force keyboard to hide on input field.
    element.attr('disabled', 'true'); // Force keyboard to hide on textarea field.
    setTimeout(function() {
        element.blur();  //actually close the keyboard
        // Remove readonly attribute after keyboard is hidden.
        element.removeAttr('readonly');
        element.removeAttr('disabled');
    }, 100);
}

// these makers should be combined into one function!

function iconMaker(iconset, icon){
    icon = (typeof icon == "undefined")?'nothing':icon;
    $('#'+iconset).css('background-image','url(<?php print constant("SYSURL") ?>/images/'+icon+'.png)');
}
function badgeMaker(iconset, badge){
      badge = (typeof badge == "undefined")?'nothing':badge; 
      $('#'+iconset).attr('src','<?php print constant("SYSURL") ?>/images/overlay_'+badge+'.png');
}
function miniMaker(iconset, mini){
      mini = (typeof mini == "undefined")?'nothing':mini;    
      $('#'+iconset).attr('src','<?php print constant("SYSURL") ?>/images/'+mini+'.png');
}
function extensionMaker(iconset, icon){
    icon = (typeof icon == "undefined")?'nothing':icon;
    $('#'+iconset).attr('src','<?php print constant("SYSURL") ?>/images/file_extension_'+icon+'.png');
}
function buttonbar(holder, name){
  var buttons='<span id="'+name+'" class="buttonbar"><a href="#delete:'+holder+'" class="minicon delete "><img class="EXdelete" src="images/delete.png"></a><a href="#info:'+holder+'" class="minicon info" "><img src="images/zoom.png"></a></span>';
  //console.log(buttons + " | "+holder);


  return buttons;

}
function listCount(val){
  $('#itemcount').html($('#sortable li').length-val);
}

function activatebuttonbar(){
    $(".delete").unbind().bind({'click': function(){
        $(this).parent("span").parent('li').effect("fade", {}, 500, function(){
                    $(this).remove();
                    listCount(0);
                });
        }
      });

    $('.info').unbind().bind({'click': function(){
      $(this).parent('span').parent('li').find('a.file').click()
    }
  })
    listCount(0);
}

function addToCart(event, ui ){
        var buttons=buttonbar(this);
        $( this ).find( ".placeholder" ).remove();
        $('.buttons').removeClass('hidden');
        $( "<li></li>" )
        .html( ui + buttons )
        .appendTo( this )
        .addClass("cart-item"); 
}

function buttonbarAction(type){

}

function showPlaylist(){
   $('.accordion-body.playlist').each(function(){
        if ($(this).hasClass('in')) {
          $('.accordion-body.schedule').collapse('hide');
          $('.accordion-body.console').collapse('hide');
        } else {
          $('.accordion-body.schedule').collapse('hide');
          $('.accordion-body.console').collapse('hide');
          $('.accordion-body.playlist').collapse('show');
        }
    })
}



var browserCheck = {
// SRC -> http://www.quirksmode.org/js/detect.html
  init: function () {
    this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
    this.version = this.searchVersion(navigator.userAgent)
      || this.searchVersion(navigator.appVersion)
      || "an unknown version";
    this.OS = this.searchString(this.dataOS) || "an unknown OS";
  },
  searchString: function (data) {
    for (var i=0;i<data.length;i++) {
      var dataString = data[i].string;
      var dataProp = data[i].prop;
      this.versionSearchString = data[i].versionSearch || data[i].identity;
      if (dataString) {
        if (dataString.indexOf(data[i].subString) != -1)
          return data[i].identity;
      }
      else if (dataProp)
        return data[i].identity;
    }
  },
  searchVersion: function (dataString) {
    var index = dataString.indexOf(this.versionSearchString);
    if (index == -1) return;
    return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
  },
  dataBrowser: [
    {
      string: navigator.userAgent,
      subString: "Chrome",
      identity: "Chrome"
    },
    {   string: navigator.userAgent,
      subString: "OmniWeb",
      versionSearch: "OmniWeb/",
      identity: "OmniWeb"
    },
    {
      string: navigator.vendor,
      subString: "Apple",
      identity: "Safari",
      versionSearch: "Version"
    },
    {
      prop: window.opera,
      identity: "Opera",
      versionSearch: "Version"
    },
    {
      string: navigator.vendor,
      subString: "iCab",
      identity: "iCab"
    },
    {
      string: navigator.vendor,
      subString: "KDE",
      identity: "Konqueror"
    },
    {
      string: navigator.userAgent,
      subString: "Firefox",
      identity: "Firefox"
    },
    {
      string: navigator.vendor,
      subString: "Camino",
      identity: "Camino"
    },
    {   // for newer Netscapes (6+)
      string: navigator.userAgent,
      subString: "Netscape",
      identity: "Netscape"
    },
    {
      string: navigator.userAgent,
      subString: "MSIE",
      identity: "Explorer",
      versionSearch: "MSIE"
    },
    {
      string: navigator.userAgent,
      subString: "Gecko",
      identity: "Mozilla",
      versionSearch: "rv"
    },
    {     // for older Netscapes (4-)
      string: navigator.userAgent,
      subString: "Mozilla",
      identity: "Netscape",
      versionSearch: "Mozilla"
    }
  ],
  dataOS : [
    {
      string: navigator.platform,
      subString: "Win",
      identity: "Windows"
    },
    {
      string: navigator.platform,
      subString: "Mac",
      identity: "Mac"
    },
    {
         string: navigator.userAgent,
         subString: "iPhone",
         identity: "iPhone/iPod"
      },
    {
      string: navigator.platform,
      subString: "Linux",
      identity: "Linux"
    }
  ]

};
browserCheck.init();

function getServerTime(){
var xhr = $.ajax('<?php print constant("SYSURL") ?>/Timer',{}
  ).done(function(data){
    $('#serverTime').html(xhr.responseText);
  }).error(function (){
    $('#serverTime').html('<span class="black heavy notextdeco">&nbsp;&nbsp;Lost connection.</span>');
  });
}

function bash(cmd,now,message){

  if (now===1){
      $('input#terminal').val(cmd).focus();
      $('.terminalResponse').html("");
      bashSubmit();
    } else {
      $('input#terminal').val(cmd).focus();
      $('.terminalResponse').html(message);
    }
}


function bashSubmit() {
          var xhr3 = $.post('<?php print constant("SYSURL") ?>/Pseudoshell',{
          arg:$('input#terminal').val(),
          time:$('#browserTime').html(),
          browser:browserCheck.browser+" "+browserCheck.version+" "+browserCheck.OS,
          password:$('input#newPassword').attr('password')
          })
          .fail(function() { 
          $('.terminalResponse').html('Parse error.');
          $('input#terminal').focus();
          })
          .done(function() {
          $('.terminalResponse').html(xhr3.responseText);
          $('input#terminal').focus();
          });

}


function makeTime(){

  var serverTime=getServerTime();
              var curr = new Date();

  var y = curr.getFullYear();

var day = curr.getDate();
if (day.toString().length < 2) { day = "0" + day;}
var mo = parseInt(curr.getMonth());
mo = mo +++ 1;
if (mo.toString().length < 2) { mo = "0" + mo;}

var h = curr.getHours();
if (h.toString().length < 2) { h = "0" + h;}

var mi = curr.getMinutes();
if (mi.toString().length < 2) { mi = "0" + mi;}

var s = curr.getSeconds();
if (s.toString().length < 2) { s = "0" + s;}
  //var serverTime=
  //console.log('Load was performed.');

$('#browserTime').html(+ y+'-'+mo+'-'+day+' '+h+':'+mi+':'+s);

//          $('#timeNow').html( Date());
          // switch this out for <?php time() ?>

}

// this has to be functionalized because we need to reinitialize mobiscroll when Stop type changes
function timePick() {

 $('.timePick.off.Stop').mobiscroll({
 //     onBeforeShow: function() {
 //       checkEndType($(this).id,'on');
 //     },
      mode: "scroller",
      display: "modal",
      animate: "pop",
      preset: 'time',
      timeFormat: 'HH:ii',
      timeWheels: 'HHii',
      showOnFocus: false,
        onClose: function(){
       $('.timePick.off.Stop, .dw').blur()

          $(window).unbind('.dw');
      }
});
  $('.timePick.off.Length').mobiscroll({

      mode: "scroller",
      display: "modal",
      animate: "pop",
      preset: 'time',
      timeFormat: 'HH.ii',
      timeWheels: 'HHii',     
      showOnFocus: false, 
      onClose: function(){
       $(window).unbind('.dw');
      }
});

  $('.timePick.off.Loops').mobiscroll({
      mode: "scroller",
      display: "modal",
      animate: "pop",
      showOnFocus: false,
      wheels:
       [ { 'Loops': { 1: '1', 2: '2', 3: '3',4: '4', 5: '5', 6: '6',7: '7', 8: '8', 9: '9',10: '10', 11: '11', 12: '12',13: '13', 14: '14', 15: '15',16: '16', 17: '17', 18: '18',19: '19', 20: '20', 21: '21',22: '22', 23: '23', 24: '24'} } ],

      onClose: function(){
        $(window).unbind('.dw');
      }
      });
}
$(function() {

$('#noscript').hide();
$('#browserLabel').html(browserCheck.browser+" "+browserCheck.version+" "+browserCheck.OS);

makeTime();
var refreshIntervalId = window.setInterval(makeTime,900);


      $('.EXdelete').on('click', function(){
        console.log('clicked');
        $(this).parents('tr').find('input.timePick').val("");
      })


/* -------------------------------------------------------------------- mobiscroll stuff */
  $('.datePick').mobiscroll({
    mode: "scroller",
    display: "modal",
    animate: "pop",
    preset: 'date',
    dateFormat: 'yy-mm-dd',
    dateOrder: 'yyMD dd',
    minDate: new Date(),
    showOnFocus: false,
    onClose: function(){
      $(window).unbind('.dw');
    }                                       
  });
  $('.timePick.Multiday').mobiscroll({
      mode: "scroller",
      display: "modal",
      animate: "pop",
      preset: 'datetime',
      dateFormat: 'yy-mm-dd ',
      minDate: new Date(),
      dateOrder: 'yyMD dd',
      timeWheels: 'HHii',
      timeFormat: 'HH:ii',
      showOnFocus: false,
      onClose: function(){
       $(window).unbind('.dw');
      }
  });

 $('.timePick.on').mobiscroll({
      mode: "scroller",
      display: "modal",
      animate: "pop",
      preset: 'time',
      timeWheels: 'HHii',
      timeFormat: 'HH:ii',
      showOnFocus: false,
      onClose: function(){
       $(window).unbind('.dw');
      }                                       
  });

timePick();

// //////////////////////////////////////////////////////////////////////end mobiscroll

// global keymappings
$(document).bind({"keydown": function(e) { 
//13: enter or return
//17: control
//83: s
//81: q
//33 pageup
//34 pagedown

      if (e.keyCode == 13) {
        if ($('input#terminal').is(":focus")) {
            bashSubmit();
          } else if ($('input#newPassword').is(":focus")) {
            $('input#newPassword').blur();
           } else {
             e.preventDefault();
      }
    }
  }
});


             
$('input#newPassword').on('blur', function(){
  // we would use jquery - but the security model prevents type changing.
  //if ()
  bash('passwd',0,'Press enter to permanently change your password.<br>');
   //$('.dropdown-menu').click();

   $(this).attr('password',$(this).val());
    //.val('Change Password');
    var password = document.getElementById('newPassword');
    //password.type = 'text';
    $('input#terminal').focus();
  }).on('click', function(e){
     e.stopPropagation();
  }).on('focus', function(e){
    $(this).addClass("required")
    $(this).val("");
    // lame ass security model from jquery won't work!!!
    //   $(this).type("password");
    var password = document.getElementById('newPassword');
    password.type = 'password';
  })
              
             

//$('.col').resizable();
$('.minicon').on("elementCreated", function(){
    var iconset=this.id, 
    mini=$(this).attr('data-mini');    
    miniMaker(iconset, mini);
})

  $('img.iconset').each(function(){
    var iconset=this.id, 
    icon=$(this).attr('data-icon'), 
    badge=$(this).attr('data-badge');
    iconMaker(iconset, icon);
    badgeMaker(iconset, badge);
  })

    $('body').css('width',screen.width);

	$('#fileTree').fileTree({
	folderEvent: 'click', 
	expandSpeed: 300, 
	collapseSpeed: 150, 
	multiFolder: false 
	}, function(file) { 
    var buttons=buttonbar('file','file');
    $( "#cart ol" ).find( ".placeholder" ).remove();
    $( "<li></li>" )
    .html( buttons + file)
    .appendTo(  "#cart ol" )
    .addClass("cart-item");
    activatebuttonbar();
    showPlaylist();
    $("#cart ol").sortable();

	})

    $( "#cart ol" ).droppable({
      greedy: true,
      activeClass: "ui-state-default",
      hoverClass: "ui-state-hover",
      accept: ":not(.ui-sortable-helper)",
      drop: function( event, ui ){
        var buttons=buttonbar(ui.draggable.text(),ui.draggable.text());
        $( this ).find( ".placeholder" ).remove();
        $('.buttons').removeClass('hidden');
        $( "<li></li>" )
        .html( buttons + ui.draggable.html())
        .appendTo( this )
        .addClass("cart-item");
        activatebuttonbar();  
          $(this).removeClass('dragdropTargetHover');
          $(ui.helper).removeClass('dragdropHelperHover');
 
     //       $(this).find('.item-container').html(  buttons + ui.draggable.html() );
 
     //       if ( ui.draggable.parent().is('.drag-container') )
     //          $('.drag-container .item-container').html('Item ' + (++items));
     //       else
               // Defer removing the item after exiting the current call stack
               // so the Draggable widget can complete the drag processing
     //          setTimeout(function () { ui.draggable.remove(); }, 0);
 
         
      },
      activate: function( event,ui ){
          $(this).addClass('dragdropTargetHover');
          $(ui.helper).addClass('dragdropHelperHover');
       showPlaylist();
      },                        
      deactivate: function(event, ui){
          $(this).removeClass('dragdropTargetHover');
          $(ui.helper).removeClass('dragdropHelperHover');
      },
      out: function (event, ui){
      //    $(this).removeClass('dragdropTargetHover');
      //    $(ui.helper).removeClass('dragdropHelperHover');
      }
      
    })

    .sortable({


	items: "li:not(.placeholder)",
	placeholder: "ui-state-highlight",
  containment: "#cart",
  axis: "y",
      sort: function(event, ui) {
        $( this ).removeClass( "ui-state-default" );
        $(this).removeClass('dragdropTargetHover');
        $(ui.helper).removeClass('dragdropHelperHover');
        listCount(1);
      },
      update: function ( e, ui ) {
 
         /* Check if the drag handle is missing */
         if ( ui.item.find('.cart-item').length == 0 ) {
            /* It is so increment the item counter */
           // $('.drag-container .item-container').html('Item ' + (++items));
             
            /* 
                And setup the item so it has a drag handle and
                responds to drag events
            */

  /* ///          ui.item            
               .find('li.file')
              //    .before( $('<div class="drag-handle">') )
               //   .parent()                  
               .draggable({
                
                     cursor: 'move',
                     zIndex: 200,
                     opacity: 0.75,
                     scroll: false,
                     containment: 'window',
                     appendTo: document.body,
                     helper: 'clone',
                      
                  });
             
            /* 
               Reset the containment.  Somehow it breaks when adding
               the external item 
            */
          //  $(this).sortable('option', 'containment', 'parent');
         }
 
      }
    }).disableSelection();


//        drop: function(event, ui) {
 //           $(ui.draggable).remove();
 //       },
 //       hoverClass: "ui-state-hover",
 //       accept: '.cart-item'
    


//$('#cart').draggable({
//	handle:"h2",
//	containment:"body"
//});


$('#activatePlaylist').on('click', function(e) {

// lets make an array
var sources = [];
var sortable = $("#cart ol").html();
var boxname = $("textarea#box1").val();
var schedule = $("#scheduleType .scheduleLabel").html();

/* for adding to the database
    $('#sortable').sortable({
        update: function(event, ui) {
            var newOrder = $(this).sortable('toArray').toString();
            $.get('saveSortable.php', {order:newOrder});
        }
    });
*/
	var filelist="";
	$( "#cart ol li a.file" ).each(function(){
    filelist=filelist + $(this).attr('rel') + '\n';
		sources.push();
	})
 // sources.push(schedule);


$(".interface2."+schedule+" input").each(function(){
//sources.push($(this).mobiscroll('getValues'));
 // $(this).each('td',function(){
  if($(this).id != 'Override' || $(this).id != 'Interrupt') {
sources.push($(this).val());
}
});
sources.push('StopType: '+$('.interface2.'+schedule+' .stopType span').text());
sources.push('Override: '+$('#Override').prop('checked'));
sources.push('Interrupt: '+$('#Interrupt').prop('checked'));

//   });
		var json_text = JSON.stringify(sources, null, 2);
		console.log(sortable);

var xhr2 = $.post('<?php print constant("SYSURL") ?>/Playlistmaker',{
    boxname:boxname,
    playlist:filelist,
    schedule:schedule
    })
    .fail(function() {           
    $('.terminalResponse').html('No connection to server');
    $('input#terminal').val(null).focus();
    })
    .done(function() {
    $('.terminalResponse').html(xhr2.responseText);
    $('input#terminal').val(null).focus();
    });

// prevent it from closing the accordion
return false;

	})
$('.accordion-body.console').bind({
  click: function(){
  $('.accordion-body.schedule').collapse('hide');
  $('.accordion-body.playlist').collapse('hide');
  },
  mouseover: function(){
  $('.accordion-body.schedule').collapse('hide');
  $('.accordion-body.playlist').collapse('hide');
  }
})
$('.accordion-body.schedule').bind({
  click: function(){
  $('input#terminal').focus();
  $('.accordion-body.console').collapse('hide');
  $('.accordion-body.playlist').collapse('hide');
  },
  mouseover: function(){
  $('.accordion-body.console').collapse('hide');
  $('.accordion-body.playlist').collapse('hide');
  }
})
$('.accordion-body.playlist').bind({
  click: function(){
  $('.accordion-body.schedule').collapse('hide');
  $('.accordion-body.console').collapse('hide');
  },  
  mouseover: function(){
  $('.accordion-body.console').collapse('hide');
  $('.accordion-body.schedule').collapse('hide');
  }
})
/*
// turn off accordion while dragging from filetree
$('body').bind({"drag dragstart":function(event){
                $('#scheduleToggle').collapse('hide');
                   // $("#ts1").triggerHandler('drag');
                   // return false;
                 }
                }).bind({"dragend":function(event){
                  $('#scheduleToggle').collapse('show');
                   // $("#ts1").triggerHandler('drag');
                   // return false;
                 }
})*/

$(window).on('focus',function(){
  $(window).unbind('.dw');
})

});


</script>
