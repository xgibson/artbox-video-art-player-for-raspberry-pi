

<!-- ----------------------------------------------------------------------------------- -->

  <div class="row-fluid">
    <div class="span5 offset1">

<div class="accordion"  id="accordion2">

<div class="accordion-group">
<div  class="accordion-heading"> 
<span class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" style="color:#334!important" href="#collapse1">
<i class="icon-list"></i> PLAYLIST: 
<span id="box1" class="editable" contentEditable="true">First List</span> 

<span class="pull-right">

<span id="itemcount" class="label" >0</span>

<a href="#" id="activatePlaylist" class="btn btn-mini btn-info">
<i class="icon-check"></i> Publish
</a>

</span>

</span>

</div>
<div id="collapse1" class="accordion-body collapse playlist">
<div class="accordion-inner ">





<div id="cart">


<div class="holder"> 
<ol id="sortable">
<li class="placeholder">Drag Media Here</li>
</ol>
</div>
</div>
</div>
</div>
</div>


<div class="accordion-group">
<div  class="accordion-heading"> 
<div class="accordion-toggle " data-toggle="collapse" data-parent="#accordion2" style="color:#334!important" href="#collapse2">
<i class="icon-time"></i> SCHEDULE

<div class="dropdown pull-right label"> 
  <span class="dropdown-toggle" id="dLabel" role="button" data-toggle="dropdown">
    <i class="icon-play"></i> <span id="scheduleType">Now & Forever</span>
  </span>

  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
    <li><a href="">Now & Forever</a></li>
    <li><a href="">Daily</a></li>
    <li><a href="">Weekly</a></li>
    <li><a href="">Special</a></li>
    <li>
    <label class="checkbox" for="Override">
    <input id="Override" name="Overide" type="checkbox">Override</label>
    </input>
    </li>
    <li>
    <label class="checkbox" for="Interrupt">
    <input id="Interrupt" name="Interrupt" type="checkbox">Interrupt</label>
    </input>
    </li>  
  </ul>

</div>

</div>

</div>
<div id="collapse2" class="accordion-body collapse schedule">
<div class="accordion-inner ">
<div class="well">
  As soon as you press the PUBLISH button, your Artbox will begin to play and loop the playlist - running each and every day and night until the Sun supernovas or you publish a new playlist.
</div>

<!--
daily
start
stop

weekly
monday
tuesday
wednesday
thursday
friday
saturday
sunday
-->
<!--
<a href="#" id="exportPlaylist" class="btn">
<i class="icon-share-alt"></i> Export
</a>
-->



</div>
</div>



</div> <!-- end accordion -->

</div>


[HTML5] > playlist parser [PHP] > crontabMaker
crontab > controller.sh [shell] > [ omxloop / omxlist / mplayer ] > fifo <  [PHP]
</div>
    <div class="span5 ">


<span class="accordion-toggle" style="color:#334!important">
<i class="icon-film" href="#collapse2"></i> MEDIA
</span
  <div id="browser" class="offset1 span5 ">
    <div>
    <h2>
    </h2>
    <div id="fileTree" class="">
    </div>
  </div>

  <!--
<div class="row-fluid">
            <div class="span4">
              <h2>Heading</h2>
              <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
              <p><a class="btn" href="#">View details &raquo;</a></p>
            </div>
            <div class="span4">
              <h2>Heading</h2>
              <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
              <p><a class="btn" href="#">View details &raquo;</a></p>
            </div>
            <div class="span4">
              <h2>Heading</h2>
              <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
              <p><a class="btn" href="#">View details &raquo;</a></p>
            </div>
          </div>
-->          
</div>

</div>
</div>



<!-- ----------------------------------------------------------------------------------- -->




<!-- using this branch: https://github.com/koppor/jquery-ui-touch-punch -->

<script type="text/javascript">

function hideKeyboard(element) {
    element.attr('readonly', 'readonly'); // Force keyboard to hide on input field.
    element.attr('disabled', 'true'); // Force keyboard to hide on textarea field.
    setTimeout(function() {
        element.blur();  //actually close the keyboard
        // Remove readonly attribute after keyboard is hidden.
        element.removeAttr('readonly');
        element.removeAttr('disabled');
    }, 100);
}

// these makers should be combined into one function!

function iconMaker(iconset, icon){
    icon = (typeof icon == "undefined")?'nothing':icon;
    $('#'+iconset).css('background-image','url(<?php print constant("SYSURL") ?>/images/'+icon+'.png)');
}
function badgeMaker(iconset, badge){
      badge = (typeof badge == "undefined")?'nothing':badge; 
      $('#'+iconset).attr('src','<?php print constant("SYSURL") ?>/images/overlay_'+badge+'.png');
}
function miniMaker(iconset, mini){
      mini = (typeof mini == "undefined")?'nothing':mini;    
      $('#'+iconset).attr('src','<?php print constant("SYSURL") ?>/images/'+mini+'.png');
}
function extensionMaker(iconset, icon){
    icon = (typeof icon == "undefined")?'nothing':icon;
    $('#'+iconset).attr('src','<?php print constant("SYSURL") ?>/images/file_extension_'+icon+'.png');
}
function buttonbar(holder, name){
  var buttons='<span id="'+name+'" class="buttonbar"><a href="#delete:'+holder+'" class="minicon delete "><img src="images/delete.png"></a><a href="#info:'+holder+'" class="minicon info" "><img src="images/zoom.png"></a></span>';
  //console.log(buttons + " | "+holder);


  return buttons;

}
function listCount(val){
  $('#itemcount').html($('#sortable li').length-val);
}

function activatebuttonbar(){
    $(".delete").unbind().bind({'click': function(){
        $(this).parent("span").parent('li').effect("fade", {}, 500, function(){
                    $(this).remove();
                    listCount(0);
                });
        }
      });

    $('.info').unbind().bind({'click': function(){
      $(this).parent('span').parent('li').find('a.file').click()
    }
  })
    listCount(0);
}

function addToCart(event, ui ){
        var buttons=buttonbar(this);
        $( this ).find( ".placeholder" ).remove();
        $('.buttons').removeClass('hidden');
        $( "<li></li>" )
        .html( ui + buttons )
        .appendTo( this )
        .addClass("cart-item"); 
}

function buttonbarAction(type){

}

function showPlaylist(){
   $('.accordion-body.playlist').each(function(){
        if ($(this).hasClass('in')) {
          $('.accordion-body.schedule').collapse('hide');
          $(this).collapse('show');
        } else {
          $('.accordion-body.schedule').collapse('hide');
          $(this).collapse('show');
        }
    })
}

$(function() {
$('body').width($(window).width());
$(window).resize(function() {
$('body').width($(window).width());
});
$("span[contenteditable]").on("keydown", function(e) {
      if (e.keyCode == 13) {
          e.preventDefault();
      }
});

//$('.col').resizable();
$('.minicon').on("elementCreated", function(){
    var iconset=this.id, 
    mini=$(this).attr('data-mini');    
    miniMaker(iconset, mini);
    alert('created')
})

  $('img.iconset').each(function(){
    var iconset=this.id, 
    icon=$(this).attr('data-icon'), 
    badge=$(this).attr('data-badge');
    iconMaker(iconset, icon);
    badgeMaker(iconset, badge);
  })

    $('body').css('width',screen.width);

	$('#fileTree').fileTree({
	folderEvent: 'click', 
	expandSpeed: 300, 
	collapseSpeed: 150, 
	multiFolder: false 
	}, function(file) { 
    var buttons=buttonbar('file','file');
    $( "#cart ol" ).find( ".placeholder" ).remove();
    $( "<li></li>" )
    .html( buttons + file)
    .appendTo(  "#cart ol" )
    .addClass("cart-item");
    activatebuttonbar();
    showPlaylist();
	})

    $( "#cart ol" ).droppable({
      //disabled: true,
      greedy: true,
      activeClass: "ui-state-default",
      hoverClass: "ui-state-hover",
      accept: ":not(.ui-sortable-helper)",
      /*drop: function( event, ui ){
        var buttons=buttonbar(ui.draggable.text(),ui.draggable.text());
        $( this ).find( ".placeholder" ).remove();
        $('.buttons').removeClass('hidden');
        $( "<li></li>" )
        .html( buttons + ui.draggable.html())
        .appendTo( this )
        .addClass("cart-item");
        activatebuttonbar();  
          $(this).removeClass('dragdropTargetHover');
          $(ui.helper).removeClass('dragdropHelperHover');
 
     //       $(this).find('.item-container').html(  buttons + ui.draggable.html() );
 
     //       if ( ui.draggable.parent().is('.drag-container') )
     //          $('.drag-container .item-container').html('Item ' + (++items));
     //       else
               // Defer removing the item after exiting the current call stack
               // so the Draggable widget can complete the drag processing
     //          setTimeout(function () { ui.draggable.remove(); }, 0);
 
         
      },*/      activate: function( event,ui ){
          $(this).addClass('dragdropTargetHover');
          $(ui.helper).addClass('dragdropHelperHover');
       showPlaylist();
      },                        
      deactivate: function(event, ui){
          $(this).removeClass('dragdropTargetHover');
          $(ui.helper).removeClass('dragdropHelperHover');
      },
      out: function (event, ui){
      //    $(this).removeClass('dragdropTargetHover');
      //    $(ui.helper).removeClass('dragdropHelperHover');
      
      },                        
      over: function(event, ui){
          $(this).addClass('dragdropTargetHover');
          $(ui.helper).addClass('cart-item');
        var buttons=buttonbar(ui.draggable.text(),ui.draggable.text());
        $( this ).find( ".placeholder" ).remove();
        $('.buttons').removeClass('hidden');
        
        activatebuttonbar();  
          $(this).removeClass('dragdropTargetHover');
          $(ui.helper).removeClass('dragdropHelperHover');
          $('.buttons').appendTo(ui.draggable);
          $( ui.draggable).appendTo(this).sortable({

items: "li:not(.placeholder)",
  placeholder: "ui-state-highlight",
  containment: "#cart ",
  axis: "y",
      sort: function() {
        $( this ).removeClass( "ui-state-default" );
        listCount(1);
      },


          })
      },
      out: function (event, ui){
        //  $(this).removeClass('dragdropTargetHover');
        //  $(ui.helper).removeClass('dragdropHelperHover');

      }
      
    })

    .sortable({


	items: "li:not(.placeholder)",
	placeholder: "ui-state-highlight",
  containment: "#cart ",
  axis: "y",
      sort: function() {
        $( this ).removeClass( "ui-state-default" );
        listCount(1);
      },
      update: function ( e, ui ) {
 
         /* Check if the drag handle is missing */
         if ( ui.item.find('.cart-item').length == 0 ) {
            // alert('found');
            /* It is so increment the item counter */
           // $('.drag-container .item-container').html('Item ' + (++items));
             
            /* 
                And setup the item so it has a drag handle and
                responds to drag events
            */
            ui.item            
               .find('li.file')
              //    .before( $('<div class="drag-handle">') )
               //   .parent()                  
               .draggable({
                
                     cursor: 'move',
                     zIndex: 200,
                     opacity: 0.75,
                     scroll: false,
                     containment: 'window',
                     appendTo: document.body,
                     helper: 'clone',
                      
                  });
             
            /* 
               Reset the containment.  Somehow it breaks when adding
               the external item 
            */
            $(this).sortable('option', 'containment', 'parent');
         }
 
      }
    })
    //disableSelection();


//        drop: function(event, ui) {
 //           $(ui.draggable).remove();
 //       },
 //       hoverClass: "ui-state-hover",
 //       accept: '.cart-item'
    


//$('#cart').draggable({
//	handle:"h2",
//	containment:"body"
//});
$('#activatePlaylist').on('click', function(e) {

var sources = [];
var sortable = $("#cart ol").html();
var boxname = $("#box1").html();

/* for adding to the database


    $('#sortable').sortable({
        update: function(event, ui) {
            var newOrder = $(this).sortable('toArray').toString();
            $.get('saveSortable.php', {order:newOrder});
        }
    });
*/
	
	$( "#cart ol li a.file" ).each(function(){
		sources.push($(this).attr('rel'));
	})
		var json_text = JSON.stringify(sources, null, 2);
		alert("'"+boxname+"':\n"+json_text);
		console.log(sortable);

// prevent it from closing the accordion
return false;

	})

  /*
$('[contentEditable]').on('blur',function(element){
    element.attr('readonly', 'readonly'); // Force keyboard to hide on input field.
    element.attr('disabled', 'true'); // Force keyboard to hide on textarea field.
    setTimeout(function() {
        element.blur();  //actually close the keyboard
        // Remove readonly attribute after keyboard is hidden.
        element.removeAttr('readonly');
        element.removeAttr('disabled');
    }, 100);
}) */





});


</script>
