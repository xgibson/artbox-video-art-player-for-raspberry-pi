<?php

#####################################################
# MODULE: 	config.php
# PURPOSE: 	navigate between requests
# USAGE: 	all files redirect here first  
# USED BY:  .htaccess
# LICENSE:  MIT or equivalent
# AUTHOR:   denjell
# VERSION:  0.6.1
# PROJECT:	ARTBOX.IO
#####################################################

define('SYSPATH',"/var/www");
define('SYSURL',"https://".$_SERVER['SERVER_NAME']);
$ApiSearchLength ="2";

# THIS IS DANGEROUS! MUST SET TO HOSTNAME / IP ADDRESS TO PREVENT CROSS SITE SCRIPTING.
header('Access-Control-Allow-Origin: https://'.$_SERVER['SERVER_NAME']); 

function get_full_url() {
	$https = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off';
	return
	    ($https ? 'https://' : 'http://').
	    (!empty($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'].'@' : '').
	    (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ($_SERVER['SERVER_NAME'].
	    ($https && $_SERVER['SERVER_PORT'] === 443 ||
	    $_SERVER['SERVER_PORT'] === 80 ? '' : ':'.$_SERVER['SERVER_PORT']))).
	    substr($_SERVER['SCRIPT_NAME'],0, strrpos($_SERVER['SCRIPT_NAME'], '/'));
}


# GET THE SERVER VALS
$request 		= $_SERVER['REQUEST_URI'];
$path 			= $_SERVER['PATH_INFO'];
$server_name 	= $_SERVER['SERVER_NAME'];
$host 			= $_SERVER['HTTP_HOST'];
$user_agent 	= $_SERVER['HTTP_USER_AGENT'];
$user_ip 		= $_SERVER['REMOTE_ADDR'];
$time 			= $_SERVER['REQUEST_TIME'];
$session 		= session_id();

# turn this off f
if ($request != "/Timer") {
	$log_message = date('Y-m-d H:i.s')." | ".$user_ip." | ".$host.$request."\n";
	error_log($log_message, 3, "/home/pi/.artbox/logs/http.log");
}