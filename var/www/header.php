<?php # header.php 

# prove that we are in a website
$special="verytrue";

?>
<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,user-scalable=0">



<meta charset="UTF-8">
<meta name="description" content="Artbox takes art out of the box and onto the wall.">
<title>
  [ARTBOX]
</title>
<meta name="author" content="Denjell 2013">
<link rel="icon" href="<?php print constant("SYSURL") ?>/favicon.ico" type="image/x-icon"> 
<link rel="shortcut icon" href="<?php print constant("SYSURL") ?>/favicon.ico" type="image/x-icon">

<link href="<?php print constant("SYSURL") ?>/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php print constant("SYSURL") ?>/css/jqueryFileTree.css" />

<link href="<?php print constant("SYSURL") ?>/css/icons.css" rel="stylesheet" />
<link href="<?php print constant("SYSURL") ?>/css/table.css" rel="stylesheet" />
<!--<link href="<?php print constant("SYSURL") ?>/css/bootstrap-responsive.css" rel="stylesheet">-->
<link href="<?php print constant("SYSURL") ?>/dependencies/mobiscroll/css/mobiscroll.core.css" rel="stylesheet" type="text/css" />
<link href="<?php print constant("SYSURL") ?>/dependencies/mobiscroll/css/mobiscroll.animation.css" rel="stylesheet" type="text/css" />
<link href="<?php print constant("SYSURL") ?>/dependencies/mobiscroll/css/mobiscroll.android-ics.css" rel="stylesheet" type="text/css" />
<link href="<?php print constant("SYSURL") ?>/css/main.css" rel="stylesheet">
<link href="<?php print constant("SYSURL") ?>/css/user.css" rel="stylesheet">

<style type="text/css">
body {
  padding-top: 60px;
  padding-bottom: 40px;
}

</style>



<!-- don't just update - we're using an extended bootstrap!!! 
http://jasny.github.io/bootstrap/
-->
<script src="<?php print constant("SYSURL") ?>/js/jquery-1.8.3.js"></script>
<script src="<?php print constant("SYSURL") ?>/js/bootstrap.js"></script>
<script src="<?php print constant("SYSURL") ?>/js/jquery-ui.js"></script>
<script src="<?php print constant("SYSURL") ?>/js/jquery.ui.touch-punch.js"></script>
<script src="<?php print constant("SYSURL") ?>/js/general.js"></script>
<script src="<?php print constant("SYSURL") ?>/js/jqueryFileTree.js.php"></script>
<script src="<?php print constant("SYSURL") ?>/js/jquery.fitvids.js"></script>
<script src="<?php print constant("SYSURL") ?>/dependencies/mobiscroll/js/mobiscroll.core.js" type="text/javascript"></script>
<script src="<?php print constant("SYSURL") ?>/dependencies/mobiscroll/js/mobiscroll.datetime.js" type="text/javascript"></script>
<script src="<?php print constant("SYSURL") ?>/dependencies/mobiscroll/js/mobiscroll.select.js" type="text/javascript"></script>
<script src="<?php print constant("SYSURL") ?>/dependencies/mobiscroll/js/mobiscroll.android-ics.js" type="text/javascript"></script>
<script src="<?php print constant("SYSURL") ?>/js/box.js.php"></script>

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="<?php print constant("SYSURL") ?>/js/html5shiv.js"></script>
<![endif]-->

</head>
<body spellcheck="false">