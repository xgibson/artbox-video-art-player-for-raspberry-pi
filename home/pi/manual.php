<pre>
THE DEFINITIVE ARTBOX MANUAL
----------------------------
TOC

QUICK START

IMPORTANT!!! Before you plug in your Artbox, you should prepare your media files. Export your video files to h264 format in an mp4 or mov container. The maximum resolution is 1920x1080. We get great results with 25 frames per second (fps), but 29.97 is fine too. 

Put the media files you want to play in the top folder of your USB stick. Your stick can be NTFS, FAT, EXT-3 or even NFS [but probably not NFS+ Encrypted]. If you put more than one media file in the top folder you will see that there is a short black break between the clips. A single file will loop perfectly.

Be wary of using external hard drives unless they have a seperate power supply! The Artbox can output a maximum of 500mA to USB, and if your device(s) take more than that the chances are good that your Artbox will reboot. :(

Gently insert your SD Card into the Artbox as you can see in the drawing.

Now insert your USB stick into one of the free slots on the Artbox, plug in your HDMI cable and audio jack, and attach the micro-usb cable to its socket next to the SD Card. 

You should see the Artbox logo appear and you should be seeing your Art RDQ (Really Darn Quick).

1) Adding Media
2) Creating a Playlist
3) Preparing Schedules
4) Using the Console
5) Edge Cases
6) Licenses
</pre>

<style>

pre {
 white-space: pre-wrap;       /* css-3 */
 white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
 white-space: -pre-wrap;      /* Opera 4-6 */
 white-space: -o-pre-wrap;    /* Opera 7 */
 word-wrap: inherit;       /* Internet Explorer 5.5+ */
}

</style>