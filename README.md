# Artbox Video Art Player (for Raspberry Pi)

This is the public fork of the master branch for the core of the Artbox Video Art Player - a simple and robust media player 
for artists, curators and collectors. Artbox has been created with the support of a large international collective of artists, 
collectors, institutions and hackers who use open source techniques and technologies. BIG THANKS to all our supporters during 
the Artbox 2013 crowdfunding campaign for helping us develop Artbox.

## Download

Get the latest ISO from the Artbox Shop, and support Artbox and Artists in the process:

http://shop.artbox.io/collections/featured/products/artbox-0-6-3-iso-download

## Project Team

- Alex Gibson (Founder): alex@artboxcloud.com
- Denjell (Lead Developer)
- Michael Meneghetti (Curator)